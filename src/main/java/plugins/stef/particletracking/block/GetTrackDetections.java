package plugins.stef.particletracking.block;

import java.util.ArrayList;
import java.util.List;

import icy.gui.dialog.MessageDialog;
import icy.main.Icy;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import icy.roi.BooleanMask3D;
import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.type.point.Point3D;
import icy.type.point.Point5D;
import plugins.adufour.activecontours.ActiveContour;
import plugins.adufour.activecontours.ActiveContours.ROIType;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.fab.trackmanager.TrackSegment;
import plugins.kernel.roi.descriptor.measure.ROIMassCenterDescriptorsPlugin;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi3d.ROI3DArea;
import plugins.kernel.roi.roi3d.ROI3DPoint;
import plugins.nchenouard.particletracking.DetectionSpotTrack;
import plugins.nchenouard.particletracking.legacytracker.SpotTrack;
import plugins.nchenouard.particletracking.legacytracker.associationMethod.Track;
import plugins.nchenouard.spot.Detection;
import plugins.nchenouard.spot.Spot;

/**
 * Block (Protocols) to get the {@link Detection} objects from a {@link Track}
 * 
 * @author Stephane Dallongeville
 */
public class GetTrackDetections extends Plugin implements Block, PluginBundled
{
    public final Var<TrackSegment> track;
    public final VarBoolean wantVirtual;
    public final VarBoolean usePreviousMask;
    public final VarROIArray rois;

    public GetTrackDetections()
    {
        super();

        track = new Var<TrackSegment>("Track (TrackSegment)", new TrackSegment());
        wantVirtual = new VarBoolean("Want virtual", Boolean.FALSE);
        usePreviousMask = new VarBoolean("Use previous mask if empty", Boolean.FALSE);
        rois = new VarROIArray("Detections (as ROIs)");
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("track", track);
        inputMap.add("wantVirtual", wantVirtual);
        inputMap.add("usePreviousMask", usePreviousMask);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("rois", rois);
    }

    @Override
    public void run()
    {
        final List<ROI> result = new ArrayList<ROI>();
        final TrackSegment ts = track.getValue();
        final boolean wv = wantVirtual.getValue();
        final boolean upm = usePreviousMask.getValue();

        // we have an input detectionResult ?
        if (ts != null)
        {
            ROI lastMaskROI = null;
            int index = 0;

            try
            {
                for (Detection detection : ts.getDetectionList())
                {
                    final ROI roi = getROI(detection, wv, upm, lastMaskROI);

                    if (roi != null)
                    {
                        // set ROI name
                        roi.setName("Track #" + ts.getId() + " Detection #" + index);
                        // add to list
                        result.add(roi);

                        // mask ROI ? --> save it
                        if ((roi instanceof ROI2DArea) || (roi instanceof ROI3DArea))
                            lastMaskROI = roi;
                    }

                    index++;
                }
            }
            catch (InterruptedException e)
            {
                if (!Icy.getMainInterface().isHeadLess())
                    MessageDialog.showDialog("Get track detections process interrupted !");
                System.err.print("Get track detections process interrupted !");

                rois.setValue(null);

                return;
            }

        }

        rois.setValue(result.toArray(new ROI[result.size()]));
    }

    private ROI getROI(Detection detection, boolean wantVirtual, boolean usePreviousMask, ROI previousMask)
            throws InterruptedException
    {
        if (detection == null)
            return null;

        if (!wantVirtual && (detection.getDetectionType() == Detection.DETECTIONTYPE_VIRTUAL_DETECTION))
            return null;

        ROI result = null;

        if (detection instanceof DetectionSpotTrack)
        {
            final DetectionSpotTrack dstDetection = (DetectionSpotTrack) detection;
            final Spot spot = dstDetection.spot;

            if (spot != null)
            {
                final Point3D[] points = getPoints(spot.point3DList);

                if (points.length > 0)
                    result = new ROI3DArea(new BooleanMask3D(points));
            }
        }
        else if (detection instanceof SpotTrack)
        {
            final SpotTrack stDetection = (SpotTrack) detection;
            final Point3D[] points = getPoints(stDetection.getPoint3DList());

            if (points.length > 0)
                result = new ROI3DArea(new BooleanMask3D(points));

        }
        else if (detection instanceof ActiveContour)
            result = ((ActiveContour) detection).toROI(ROIType.AREA, null);

        // no yet done ? --> create a single point 3D ROI
        if (result == null)
            result = new ROI3DPoint(detection.getX(), detection.getY(), detection.getZ());

        // special case where we don't have mask and we want to have one (using the previous one)
        if ((result instanceof ROI3DPoint) && usePreviousMask && (previousMask != null))
        {
            final ROI newResult = previousMask.getCopy();
            final Point5D oldPos = ROIMassCenterDescriptorsPlugin.computeMassCenter(newResult);
            final Point5D curPos = result.getPosition5D();

            // adjust position from the virtual detection position
            if (newResult instanceof ROI2DArea)
                ((ROI2DArea) newResult).translate(curPos.getX() - oldPos.getX(), curPos.getY() - oldPos.getY());
            else if (newResult instanceof ROI3DArea)
                ((ROI3DArea) newResult).translate(curPos.getX() - oldPos.getX(), curPos.getY() - oldPos.getY(),
                        curPos.getZ() - oldPos.getZ());

            // use this ROI as result
            result = newResult;
        }

        if (result instanceof ROI2D)
            ((ROI2D) result).setT(detection.getT());
        else if (result instanceof ROI3D)
            ((ROI3D) result).setT(detection.getT());

        return result;
    }

    private Point3D[] getPoints(List<plugins.nchenouard.spot.Point3D> point3dList)
    {
        if (point3dList == null)
            return new Point3D[0];

        final Point3D[] result = new Point3D[point3dList.size()];
        for (int i = 0; i < result.length; i++)
        {
            final plugins.nchenouard.spot.Point3D pt = point3dList.get(i);
            result[i] = new Point3D.Double(pt.x, pt.y, pt.z);
        }

        return result;
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
