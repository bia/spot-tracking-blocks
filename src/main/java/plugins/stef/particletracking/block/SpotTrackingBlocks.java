package plugins.stef.particletracking.block;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Base plugin class for Spot tracking blocks (Protocols)
 * 
 * @author Stephane
 */
public class SpotTrackingBlocks extends Plugin implements PluginLibrary
{
    public SpotTrackingBlocks()
    {
        super();
    }
}
