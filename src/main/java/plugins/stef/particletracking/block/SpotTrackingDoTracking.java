/**
 * 
 */
package plugins.stef.particletracking.block;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.fab.trackmanager.TrackGroup;
import plugins.nchenouard.particletracking.MHTparameterSet;
import plugins.nchenouard.particletracking.SpotTracking;
import plugins.nchenouard.spot.DetectionResult;

/**
 * Block (Protocols) to do the Spot Tracking from a given set of Detections.
 * 
 * @author Stephane Dallongeville
 */
public class SpotTrackingDoTracking extends Plugin implements Block, PluginBundled
{
    public final Var<DetectionResult> detections;
    public final Var<MHTparameterSet> parameterSet;
    public final VarBoolean useLPSolver;
    public final VarBoolean showProgress;
    public final Var<TrackGroup> tracks;

    public SpotTrackingDoTracking()
    {
        super();

        detections = new Var<DetectionResult>("Detections", new DetectionResult());
        parameterSet = new Var<MHTparameterSet>("MHT parameters", new MHTparameterSet());
        useLPSolver = new VarBoolean("Use LPSolver", Boolean.TRUE);
        showProgress = new VarBoolean("Show progress", Boolean.FALSE);
        tracks = new Var<TrackGroup>("TrackGroup", new TrackGroup(null));
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("detections", detections);
        inputMap.add("parameterSet", parameterSet);
        inputMap.add("useLPSolver", useLPSolver);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("tracks", tracks);
    }

    @Override
    public void run()
    {
        final DetectionResult detectionResult = detections.getValue();
        final MHTparameterSet parameters = parameterSet.getValue();

        if ((detectionResult != null) && (parameters != null))
        {
            // probably better to affect correct detection results first
            parameters.detectionResults = detectionResult;
            tracks.setValue(SpotTracking.executeTracking(parameters, detectionResult,
                    useLPSolver.getValue().booleanValue(), true, showProgress.getValue().booleanValue()));
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
