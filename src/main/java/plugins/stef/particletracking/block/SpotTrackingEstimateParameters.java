/**
 * 
 */
package plugins.stef.particletracking.block;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.nchenouard.particletracking.MHTparameterSet;
import plugins.nchenouard.particletracking.SpotTracking;
import plugins.nchenouard.spot.DetectionResult;

/**
 * Block (Protocols) to use the automatic Spot Tracking parameters estimation.
 * 
 * @author Stephane Dallongeville
 */
public class SpotTrackingEstimateParameters extends Plugin implements Block, PluginBundled
{
    public final Var<DetectionResult> detections;
    public final VarBoolean directedMotion;
    public final VarBoolean multiMotionModel;
    public final VarBoolean updateMotion;
    public final Var<MHTparameterSet> parameterSet;

    public SpotTrackingEstimateParameters()
    {
        super();

        detections = new Var<DetectionResult>("Detections", new DetectionResult());

        multiMotionModel = new VarBoolean("Multi motion model", Boolean.FALSE);
        directedMotion = new VarBoolean("Directed motion", Boolean.FALSE);
        updateMotion = new VarBoolean("Update motion", Boolean.FALSE);

        parameterSet = new Var<MHTparameterSet>("MHT parameters", new MHTparameterSet());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("detections", detections);
        inputMap.add("multiMotionModel", multiMotionModel);
        inputMap.add("directedMotion", directedMotion);
        inputMap.add("updateMotion", updateMotion);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("parameterSet", parameterSet);
    }

    @Override
    public void run()
    {
        final DetectionResult detectionResult = detections.getValue();

        // we have an input detectionResult ?
        if (detectionResult != null)
        {
            parameterSet.setValue(SpotTracking.estimateParameters(detectionResult, directedMotion.getValue()
                    .booleanValue(), !multiMotionModel.getValue().booleanValue(), updateMotion.getValue()
                    .booleanValue()));
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
