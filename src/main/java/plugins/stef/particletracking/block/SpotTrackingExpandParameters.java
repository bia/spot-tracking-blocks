/**
 * 
 */
package plugins.stef.particletracking.block;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarString;
import plugins.nchenouard.particletracking.MHTparameterSet;

/**
 * Block (Protocols) to expand the Spot Tracking MHTParameter object into variables.
 * 
 * @author Stephane
 */
public class SpotTrackingExpandParameters extends Plugin implements Block, PluginBundled
{
    public final Var<MHTparameterSet> parameterSet;

    // MHT algorithm
    public final VarDouble gateFactor;
    public final VarInteger mhtDepth;
    public final VarDouble numberInitialObj;
    public final VarDouble numberNewObj;

    public final VarInteger numberFalseDetection;
    public final VarDouble detectionRate;
    public final VarDouble meanTrackLength;

    // motion parameters
    public final VarBoolean multiMotionModel;

    public final VarDouble dispXY;
    public final VarDouble dispZ;
    public final VarBoolean directedMotion;
    public final VarBoolean updateMotion;

    public final VarDouble dispXY2;
    public final VarDouble dispZ2;
    public final VarBoolean directedMotion2;
    public final VarBoolean updateMotion2;

    public final VarBoolean useMostLikelyModel;
    public final VarDouble immInertia;

    // target existence parameters
    public final VarDouble confirmationThreshold;
    public final VarDouble terminationThreshold;

    // tracks output
    public final VarString trackGroupName;

    public SpotTrackingExpandParameters()
    {
        super();

        parameterSet = new Var<MHTparameterSet>("MHT parameters", new MHTparameterSet());

        numberInitialObj = new VarDouble("Number of initial object", MHTparameterSet.defaultNumberInitialObjects);
        numberNewObj = new VarDouble("Number of new object per frame", MHTparameterSet.defaultNumberNewObjects);
        mhtDepth = new VarInteger("Depth of the track trees", MHTparameterSet.defaultMHTDepth);
        gateFactor = new VarDouble("Gate factor for association", MHTparameterSet.defaultGateFactor);
        numberFalseDetection = new VarInteger("Number of false detection per frame",
                MHTparameterSet.defaultNumberOfFalseDetections);
        detectionRate = new VarDouble("Probability of detection", MHTparameterSet.defaultDetectionRate);
        meanTrackLength = new VarDouble("Expected track length", MHTparameterSet.defaultMeanTrackLength);

        multiMotionModel = new VarBoolean("Multi motion model", !MHTparameterSet.defaultIsSingleMotion);

        dispXY = new VarDouble("XY displacement (1st model)", MHTparameterSet.defaultDisplacementXY);
        dispZ = new VarDouble("Z displacement (1st model)", MHTparameterSet.defaultDisplacementZ);
        directedMotion = new VarBoolean("Directed motion (1st model)", MHTparameterSet.defaultIsDirectedMotion);
        updateMotion = new VarBoolean("Update motion (1st model)", MHTparameterSet.defaultIsUpdateMotion);

        dispXY2 = new VarDouble("XY displacement (2nd model)", MHTparameterSet.defaultDisplacementXY2);
        dispZ2 = new VarDouble("Z displacement (2nd model)", MHTparameterSet.defaultDisplacementZ2);
        directedMotion2 = new VarBoolean("Directed motion (2nd model)", MHTparameterSet.defaultIsDirectedMotion2);
        updateMotion2 = new VarBoolean("Update motion (2nd model)", MHTparameterSet.defaultIsUpdateMotion2);

        useMostLikelyModel = new VarBoolean("Use most likely model (multi motion model only)",
                MHTparameterSet.defaultUseMostLikelyModel);
        immInertia = new VarDouble("Model change inertia (multi motion model only)", MHTparameterSet.defaultIMMInertia);

        confirmationThreshold = new VarDouble("Minimum existence probability for confirmation",
                MHTparameterSet.defaultConfirmationThreshold);
        terminationThreshold = new VarDouble("Existence probability for track termination",
                MHTparameterSet.defaultTerminationThreshold);

        trackGroupName = new VarString("Track group name", MHTparameterSet.defaultTrackGroupName);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("parameterSet", parameterSet);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("numberInitialObj", numberInitialObj);
        outputMap.add("numberNewObj", numberNewObj);
        outputMap.add("mhtDepth", mhtDepth);
        outputMap.add("gateFactor", gateFactor);
        outputMap.add("numberFalseDetection", numberFalseDetection);
        outputMap.add("detectionRate", detectionRate);
        outputMap.add("meanTrackLength", meanTrackLength);

        outputMap.add("multiMotionModel", multiMotionModel);

        outputMap.add("dispXY", dispXY);
        outputMap.add("dispZ", dispZ);
        outputMap.add("directedMotion", directedMotion);
        outputMap.add("updateMotion", updateMotion);

        outputMap.add("dispXY2", dispXY2);
        outputMap.add("dispZ2", dispZ2);
        outputMap.add("directedMotion2", directedMotion2);
        outputMap.add("updateMotion2", updateMotion2);

        outputMap.add("useMostLikelyModel", useMostLikelyModel);
        outputMap.add("immInertia", immInertia);

        outputMap.add("confirmationThreshold", confirmationThreshold);
        outputMap.add("terminationThreshold", terminationThreshold);

        outputMap.add("trackGroupName", trackGroupName);
    }

    @Override
    public void run()
    {
        final MHTparameterSet mhtParameters = parameterSet.getValue();

        // we have an input parameter set ?
        if (mhtParameters != null)
        {
            gateFactor.setValue(Double.valueOf(mhtParameters.gateFactor));
            mhtDepth.setValue(Integer.valueOf(mhtParameters.mhtDepth));
            numberInitialObj.setValue(Double.valueOf(mhtParameters.numberInitialObjects));
            numberNewObj.setValue(Double.valueOf(mhtParameters.numberNewObjects));

            numberFalseDetection.setValue(Integer.valueOf(mhtParameters.numberOfFalseDetections));
            detectionRate.setValue(Double.valueOf(mhtParameters.detectionRate));
            meanTrackLength.setValue(Double.valueOf(mhtParameters.meanTrackLength));

            multiMotionModel.setValue(Boolean.valueOf(!mhtParameters.isSingleMotion));

            dispXY.setValue(Double.valueOf(mhtParameters.displacementXY));
            dispZ.setValue(Double.valueOf(mhtParameters.displacementZ));
            directedMotion.setValue(Boolean.valueOf(mhtParameters.isDirectedMotion));
            updateMotion.setValue(Boolean.valueOf(mhtParameters.isUpdateMotion));

            dispXY2.setValue(Double.valueOf(mhtParameters.displacementXY2));
            dispZ2.setValue(Double.valueOf(mhtParameters.displacementZ2));
            directedMotion2.setValue(Boolean.valueOf(mhtParameters.isDirectedMotion2));
            updateMotion2.setValue(Boolean.valueOf(mhtParameters.isUpdateMotion2));

            useMostLikelyModel.setValue(Boolean.valueOf(mhtParameters.useMostLikelyModel));
            immInertia.setValue(Double.valueOf(mhtParameters.immInertia));

            confirmationThreshold.setValue(Double.valueOf(mhtParameters.confirmationThreshold));
            terminationThreshold.setValue(Double.valueOf(mhtParameters.terminationThreshold));

            trackGroupName.setValue(mhtParameters.trackGroupName);
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
