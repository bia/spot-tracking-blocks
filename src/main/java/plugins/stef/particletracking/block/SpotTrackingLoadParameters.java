/**
 * 
 */
package plugins.stef.particletracking.block;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;

import java.io.File;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.nchenouard.particletracking.MHTparameterSet;
import plugins.nchenouard.particletracking.SpotTracking;

/**
 * Block (Protocols) to load Spot Tracking parameters from a file.
 * 
 * @author Stephane Dallongeville
 */
public class SpotTrackingLoadParameters extends Plugin implements Block, PluginBundled
{
    public final VarMutable file;
    public final Var<MHTparameterSet> parameterSet;

    public SpotTrackingLoadParameters()
    {
        super();

        file = new VarMutable("File", null)
        {
            @Override
            public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
            {
                return (String.class == source.getType()) || (File.class == source.getType());
            }
        };
        parameterSet = new Var<MHTparameterSet>("MHT parameters", new MHTparameterSet());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("file", file);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("parameterSet", parameterSet);
    }

    @Override
    public void run()
    {
        final Object obj = file.getValue();

        if (obj != null)
        {
            final File f;

            if (obj instanceof String)
                f = new File((String) obj);
            else
                f = (File) obj;

            parameterSet.setValue(SpotTracking.loadParameters(f));
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
