/**
 * 
 */
package plugins.stef.particletracking.block;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;

import java.io.File;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarMutable;
import plugins.nchenouard.particletracking.MHTparameterSet;
import plugins.nchenouard.particletracking.SpotTracking;

/**
 * Block (Protocols) to save Spot Tracking parameters into a file.
 * 
 * @author Stephane Dallongeville
 */
public class SpotTrackingSaveParameters extends Plugin implements Block, PluginBundled
{
    public final VarMutable file;
    public final Var<MHTparameterSet> parameterSet;

    public SpotTrackingSaveParameters()
    {
        super();

        parameterSet = new Var<MHTparameterSet>("MHT parameters", new MHTparameterSet());
        file = new VarMutable("File", null)
        {
            @Override
            public boolean isAssignableFrom(@SuppressWarnings("rawtypes") Var source)
            {
                return (String.class == source.getType()) || (File.class == source.getType());
            }
        };
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("parameterSet", parameterSet);
        inputMap.add("file", file);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        // nothing here
    }

    @Override
    public void run()
    {
        final Object obj = file.getValue();

        if (obj != null)
        {
            final File f;

            if (obj instanceof String)
                f = new File((String) obj);
            else
                f = (File) obj;

            SpotTracking.saveParameters(parameterSet.getValue(), f);
        }
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
