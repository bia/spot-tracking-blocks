/**
 * 
 */
package plugins.stef.particletracking.block;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarInteger;
import plugins.adufour.vars.lang.VarString;
import plugins.nchenouard.particletracking.MHTparameterSet;

/**
 * Block (Protocols) to set and compact all Spot Tracking parameters into a MHTParameter object.
 * 
 * @author Stephane Dallongeville
 */
public class SpotTrackingSetParameters extends Plugin implements Block, PluginBundled
{
    // MHT algorithm
    public final VarDouble gateFactor;
    public final VarInteger mhtDepth;
    public final VarDouble numberInitialObj;
    public final VarDouble numberNewObj;

    public final VarInteger numberFalseDetection;
    public final VarDouble detectionRate;
    public final VarDouble meanTrackLength;

    // motion parameters
    public final VarBoolean multiMotionModel;

    public final VarDouble dispXY;
    public final VarDouble dispZ;
    public final VarBoolean directedMotion;
    public final VarBoolean updateMotion;

    public final VarDouble dispXY2;
    public final VarDouble dispZ2;
    public final VarBoolean directedMotion2;
    public final VarBoolean updateMotion2;

    public final VarBoolean useMostLikelyModel;
    public final VarDouble immInertia;

    // target existence parameters
    public final VarDouble confirmationThreshold;
    public final VarDouble terminationThreshold;

    // tracks output
    public final VarString trackGroupName;

    public final Var<MHTparameterSet> parameterSet;

    public SpotTrackingSetParameters()
    {
        super();

        numberInitialObj = new VarDouble("Number of initial object", MHTparameterSet.defaultNumberInitialObjects);
        numberNewObj = new VarDouble("Number of new object per frame", MHTparameterSet.defaultNumberNewObjects);
        mhtDepth = new VarInteger("Depth of the track trees", MHTparameterSet.defaultMHTDepth);
        gateFactor = new VarDouble("Gate factor for association", MHTparameterSet.defaultGateFactor);
        numberFalseDetection = new VarInteger("Number of false detection per frame",
                MHTparameterSet.defaultNumberOfFalseDetections);
        detectionRate = new VarDouble("Probability of detection", MHTparameterSet.defaultDetectionRate);
        meanTrackLength = new VarDouble("Expected track length", MHTparameterSet.defaultMeanTrackLength);

        multiMotionModel = new VarBoolean("Multi motion model", !MHTparameterSet.defaultIsSingleMotion);

        dispXY = new VarDouble("XY displacement (1st model)", MHTparameterSet.defaultDisplacementXY);
        dispZ = new VarDouble("Z displacement (1st model)", MHTparameterSet.defaultDisplacementZ);
        directedMotion = new VarBoolean("Directed motion (1st model)", MHTparameterSet.defaultIsDirectedMotion);
        updateMotion = new VarBoolean("Update motion (1st model)", MHTparameterSet.defaultIsUpdateMotion);

        dispXY2 = new VarDouble("XY displacement (2nd model)", MHTparameterSet.defaultDisplacementXY2);
        dispZ2 = new VarDouble("Z displacement (2nd model)", MHTparameterSet.defaultDisplacementZ2);
        directedMotion2 = new VarBoolean("Directed motion (2nd model)", MHTparameterSet.defaultIsDirectedMotion2);
        updateMotion2 = new VarBoolean("Update motion (2nd model)", MHTparameterSet.defaultIsUpdateMotion2);

        useMostLikelyModel = new VarBoolean("Use most likely model (multi motion model only)",
                MHTparameterSet.defaultUseMostLikelyModel);
        immInertia = new VarDouble("Model change inertia (multi motion model only)", MHTparameterSet.defaultIMMInertia);

        confirmationThreshold = new VarDouble("Minimum existence probability for confirmation",
                MHTparameterSet.defaultConfirmationThreshold);
        terminationThreshold = new VarDouble("Existence probability for track termination",
                MHTparameterSet.defaultTerminationThreshold);

        trackGroupName = new VarString("Track group name", MHTparameterSet.defaultTrackGroupName);

        parameterSet = new Var<MHTparameterSet>("MHT parameters", new MHTparameterSet());
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("numberInitialObj", numberInitialObj);
        inputMap.add("numberNewObj", numberNewObj);
        inputMap.add("mhtDepth", mhtDepth);
        inputMap.add("gateFactor", gateFactor);
        inputMap.add("numberFalseDetection", numberFalseDetection);
        inputMap.add("detectionRate", detectionRate);
        inputMap.add("meanTrackLength", meanTrackLength);

        inputMap.add("multiMotionModel", multiMotionModel);

        inputMap.add("dispXY", dispXY);
        inputMap.add("dispZ", dispZ);
        inputMap.add("directedMotion", directedMotion);
        inputMap.add("updateMotion", updateMotion);

        inputMap.add("dispXY2", dispXY2);
        inputMap.add("dispZ2", dispZ2);
        inputMap.add("directedMotion2", directedMotion2);
        inputMap.add("updateMotion2", updateMotion2);

        inputMap.add("useMostLikelyModel", useMostLikelyModel);
        inputMap.add("immInertia", immInertia);

        inputMap.add("confirmationThreshold", confirmationThreshold);
        inputMap.add("terminationThreshold", terminationThreshold);

        inputMap.add("trackGroupName", trackGroupName);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("parameterSet", parameterSet);
    }

    @Override
    public void run()
    {
        final MHTparameterSet mhtParameters = parameterSet.getValue();

        mhtParameters.gateFactor = gateFactor.getValue().doubleValue();
        mhtParameters.mhtDepth = mhtDepth.getValue().intValue();
        mhtParameters.numberInitialObjects = numberInitialObj.getValue().doubleValue();
        mhtParameters.numberNewObjects = numberNewObj.getValue().doubleValue();

        mhtParameters.numberOfFalseDetections = numberFalseDetection.getValue().intValue();
        mhtParameters.detectionRate = detectionRate.getValue().doubleValue();
        mhtParameters.meanTrackLength = meanTrackLength.getValue().doubleValue();

        mhtParameters.isSingleMotion = !multiMotionModel.getValue().booleanValue();

        mhtParameters.displacementXY = dispXY.getValue().doubleValue();
        mhtParameters.displacementZ = dispZ.getValue().doubleValue();
        mhtParameters.isDirectedMotion = directedMotion.getValue().booleanValue();
        mhtParameters.isUpdateMotion = updateMotion.getValue().booleanValue();

        mhtParameters.displacementXY2 = dispXY2.getValue().doubleValue();
        mhtParameters.displacementZ2 = dispZ2.getValue().doubleValue();
        mhtParameters.isDirectedMotion2 = directedMotion2.getValue().booleanValue();
        mhtParameters.isUpdateMotion2 = updateMotion2.getValue().booleanValue();

        mhtParameters.useMostLikelyModel = useMostLikelyModel.getValue().booleanValue();
        mhtParameters.immInertia = immInertia.getValue().doubleValue();

        mhtParameters.confirmationThreshold = confirmationThreshold.getValue().doubleValue();
        mhtParameters.terminationThreshold = terminationThreshold.getValue().doubleValue();

        mhtParameters.trackGroupName = trackGroupName.getValue();
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
