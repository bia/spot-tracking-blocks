package plugins.stef.particletracking.block;

import java.util.List;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginBundled;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarArray;
import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;

/**
 * Block (Protocols) to expand TrackGroup object to a list of track.
 * 
 * @author Stephane Dallongeville
 */
public class TrackGroupToTracks extends Plugin implements Block, PluginBundled
{
    public final Var<TrackGroup> trackgroup;
    public final VarArray<TrackSegment> tracks;

    public TrackGroupToTracks()
    {
        super();

        trackgroup = new Var<TrackGroup>("TrackGroup", new TrackGroup(null));
        tracks = new VarArray<TrackSegment>("Tracks (Array of TrackSegment)", TrackSegment[].class, new TrackSegment[0]);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("trackGroup", trackgroup);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("tracks", tracks);
    }

    @Override
    public void run()
    {
        final TrackGroup tg = trackgroup.getValue();

        // we have an input detectionResult ?
        if (tg != null)
        {
            final List<TrackSegment> trackList = tg.getTrackSegmentList();

            tracks.setValue(trackList.toArray(new TrackSegment[trackList.size()]));
        }
        else
            tracks.setValue(new TrackSegment[0]);
    }

    @Override
    public String getMainPluginClassName()
    {
        return SpotTrackingBlocks.class.getName();
    }
}
